<?php
/*
  ^  funcionalidad de marcar el caracter de inicio
  () Parentesis: agrupan caracteres
  {n,m} n indica el minimo de veces que debe aparecer, y m el maximo
  .  Punto: Indica cualquier caracteres
  \s Indica el caracter de espacio
  * Indica que la agrupacion puede aparecer 0 o más veces
  $ Indica el final de la cadena


*/
//Realizar una expresión regular que detecte emails correctos.

$email = "B3lle.aparicio3193@gmail.com";
$email2 = "Bereniceaparicio@aefcm.gob.mx";
/*Inicia cadena de caracteres
  con letras minusculas y mayuscula
  numeros del 0 al 9
  se podran agregar caracteres especiales como . _ -
  Pueden aparecer agrupaciones 0 o mas veces letras y numeros
  Debe de colocar el arroba @
  puede agregar letras minusculas y mayuscula
  despues continua un punto
  mas 
  letras minusculas y mayuscula*/

$prueba =  preg_match('/^([a-zA-Z0-9)]+[._-]*[a-zA-Z0-9])+@[a-zA-Z]+([.][a-zA-Z]+)*[.][a-zA-Z]{2,6}$/', $email2);

if ($prueba) {
  echo "<p>Email Correcto </p>";
}

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
/*REFERENCIA www.gob.mx
  La CURP contiene 18 elementos de un código alfanumérico; 16 de ellos 
  son la primer letra y primer vocal interna del primer apellido,
  primer letra del segundo apellido, primer letra del primer nombre, 
  año, mes y día de la fecha de nacimiento;
  género, 
  las dos letras del lugar de nacimiento de acuerdo al código de la Entidad Federativa.*/

$CURP = "AUZB930331MDFPXR05";
$CURPs = "ZUGB700709MHGXNL02";

$pruebaCURP = preg_match('/^([A-Z]{1}[AEIOU]{1}[A-Z]{2})+([0-9]{2}[0-3]{1}[0-9]{1}[0-9]{2})+[HM]{1}+(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)+[A-Z]{3}+[0-9]{2}$/', $CURP);

if ($pruebaCURP) {
  echo "<p> CURP Correcto </p>";
}else{
  echo "<p> CURP Incorrecto </p>";
}

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras. (([A-Za-z]){1,60}(\s+([A-Za-z]){1,60}))

$longitud = "ParangaricutirimícuaroParangaricutirimícuaroParangarinmmmmmmmmmmmmm";
$res = preg_match('/^(([a-zA-Z]){1,51}(\s+([a-zA-Z]){1,51})*)$/',$longitud);
if ($res) {
  echo "<p>Palabras de longitud mayor a 50</p>" ;
}else{
  echo "<p>Palabra de longitud menor a 50 </p>";
}

//Crea una funcion para escapar los simbolos especiales.

function simbolosEspeciales(){
  $caracter = 'Berenice esta aprendiendo ~ "<p>PHP</p>"';
  $caracter = preg_quote($caracter);
  echo $caracter;
}
//llamando a la función
simbolosEspeciales();

//Crear una expresion regular para detectar números decimales.
$numDecimal = 3103098.76;
$resNum=preg_match('/^[0-9]+([.][0-9]+)$/', $numDecimal);
  if ($resNum) {
    echo "<p> Correcto </p>";
  }else{
    echo "<p> Incorrecto </p>";
  }
?>